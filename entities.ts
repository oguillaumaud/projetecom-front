export interface User{
    id?: number,
    pseudo:string,
    email:string,
    password?:string,
    role?:string,
    coordonnes?: Coordonne[],
    comments?: Comment[],
    commandes?: Commande[]
}

export interface Commande{
    id?:number,
    auther: User,
    prix:number,
    ligneProduits?: LigneProduits[]
}

export interface LigneProduits{
    id?: number,
    produits?:Produit[],
    commande:Commande,
    quantite:number,
    prix:number
}

export interface Produit{
    id?:number,
    titre:string,
    description:string,
    prix:number,
    images?:Image[],
    categorie?:Categorie,
    ligneProduits?: LigneProduits,
    comments?: Comment[]
}

export interface Image{
    id?:number,
    src:string,
    description:string,
    prduit?:Produit
}

export interface Categorie{
    id?:number,
    nom:string
}

export interface Coordonne{
    id?:number,
    auther?: User,
    nom:string,
    prenom:string,
    adress:string,
    complement?:string,
    codePostal:number,
    ville:string,
    pays:string,
    telephone:string
}

export interface Commentaire{
    id?:number,
    text:string,
    note?:number,
    produit?:Produit | undefined | null,
    author?:User | null
}

export interface Page<T>{
    first:boolean,
    last:boolean,
    totalPages:number,
    totalElements:number,
    content?:T[]
}