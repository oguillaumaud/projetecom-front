import type { User } from "~/entities"

export const useAuth = () => {
  return useCookie<User|null>('user', {
    sameSite: 'strict'
  });
}
