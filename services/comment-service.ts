import axios from 'axios';

const API_URL = 'http://localhost:8080/api/comment';

export async function getAllComments() {
    try {
        const response = await axios.get(API_URL);
        return response.data;
    } catch (error) {
        console.error('Error fetching all comments:', error);
        throw error;
    }
}

export async function getCommentById(id: number) {
    try {
        const response = await axios.get(`${API_URL}/${id}`);
        return response.data;
    } catch (error) {
        console.error('Error fetching comment by id:', error);
        throw error;
    }
}

export async function postComment(comment: any) {
    try {
        const response = await axios.post(API_URL, comment);
        console.log(comment);
        
        return response.data;
    } catch (error) {
        console.error('Error posting comment:', error);
        throw error;
    }
}

export async function deleteComment(id: number) {
    try {
        const response = await axios.delete(`${API_URL}/${id}`);
        return response.data;
    } catch (error) {
        console.error('Error deleting comment:', error);
        throw error;
    }
}

export async function updateComment(id: number, comment: any) {
    try {
        const response = await axios.post(`${API_URL}/${id}`, comment);
        return response.data;
    } catch (error) {
        console.error('Error updating comment:', error);
        throw error;
    }
}
