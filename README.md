
Nom de projet : Blog / 

But : Créer un site de blog avec une fonction d'authentification utilisant Java pour le backend et Vue.js pour le frontend

Concept : le but principal de blog est de partager la passion pour la cuisine japonaise avec vos lecteurs. 
 
pour lancer le page blog: taper en ligne de commande dans le dossier : npm i / npm run dev.créer un site de blog avec une fonction d'authentification utilisant Java pour le backend et Vue.js pour le frontend

Thechnologie
Backend : Java / spring boot
          SQL pour base de donées 

Frontend : Vue.js(framework javascript) pour créer une interface utilisateur interactive et réactive
           Axios pour effectuer des requêtes HTTP vers votre backend Java






# Nuxt 3 Minimal Starter

Look at the [Nuxt 3 documentation](https://nuxt.com/docs/getting-started/introduction) to learn more.

## Setup

Make sure to install the dependencies:

```bash
# npm
npm install

# pnpm
pnpm install

# yarn
yarn install

# bun
bun install
```

## Development Server

Start the development server on `http://localhost:3000`:

```bash
# npm
npm run dev

# pnpm
pnpm run dev

# yarn
yarn dev

# bun
bun run dev
```

## Production

Build the application for production:

```bash
# npm
npm run build

# pnpm
pnpm run build

# yarn
yarn build

# bun
bun run build
```

Locally preview production build:

```bash
# npm
npm run preview

# pnpm
pnpm run preview

# yarn
yarn preview

# bun
bun run preview
```

Check out the [deployment documentation](https://nuxt.com/docs/getting-started/deployment) for more information.
